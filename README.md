# slstatus - Suckless Status. 

slstatus is a suckless utility made for window managers, It essentially displays
a status bar. 

## Prerequisites.
- Xlib Header Files. 
- A Window Manager like Dwm, sway, and etc..
- Git. 
- Xorg-Xinit.

## Installation. 
Clone this repository. 
```bash
git clone https://gitlab.com/crimsonmyth/slstatus.git
cd slstatus/
sudo make clean install
```
## Running it. 
I am currently only aware of running slstatus through ~/.xinitrc:  
```bash
echo "slstatus &" >> ~/.xinitrc
startx # Run it as console user. Through a TTY. 
```

